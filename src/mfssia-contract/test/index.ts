/* eslint-disable no-unused-expressions */
/* eslint-disable node/no-missing-import */
/* eslint-disable camelcase */
import { ethers, waffle } from "hardhat";
import chai from "chai";

import EntityServiceRegistryArtifact from "../artifacts/contracts/core/EntityServiceRegistry.sol/EntityServiceRegistry.json";
import { EntityServiceRegistry } from "../typechain/types/EntityServiceRegistry";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

const { deployContract } = waffle;
const { expect } = chai;

describe("EntityServiceRegistry Contract", () => {
  let owner: SignerWithAddress;
  let owner2: SignerWithAddress;
  const provider = ethers.provider;

  let entityServiceRegistryInstance: EntityServiceRegistry;

  beforeEach(async () => {
    [owner, owner2] = await ethers.getSigners();

    entityServiceRegistryInstance = (await deployContract(
      owner,
      EntityServiceRegistryArtifact,
    )) as EntityServiceRegistry;
  });
  describe("Deployment", function () {
    // If the callback function is async, Mocha will `await` it.
    it("Should set the right owner", async function () {
      // This test expects the owner variable stored in the contract to be equal
      // to our Signer's owner.
      expect(await entityServiceRegistryInstance.owner()).to.equal(owner.address);
    });
  });
  describe("Ownable", async () => {
    it("Owner is able to transfer ownership", async () => {
      await expect(entityServiceRegistryInstance.transferOwnership(owner2.address))
        .to.emit(entityServiceRegistryInstance, "OwnershipTransferred")
        .withArgs(owner.address, owner2.address);
    });
  });
  describe("Add Business Contract info on-chain", () => {
    it("Should receive business contract inputs and save on-chain", async () => {
      // const options = { gasPrice: 1000000000, gasLimit: 6700000 };
      const businessContractId = "1223";
      const price = "200";
      const deliveryInterval = "2";
      const quantity = "2";
      const productName = "iPhone";
      const tx = await entityServiceRegistryInstance.addBusinessContractInfo(
        businessContractId,
        price,
        deliveryInterval,
        quantity,
        productName,
      );
      expect(tx.hash).to.be.not.null;
      expect(await provider.getTransactionReceipt(tx.hash)).to.be.not.null;
    });

    it("Should check BusinessContractInfoCreated event is emitted ", async () => {
      const businessContractId = "1223";
      const price = "200";
      const deliveryInterval = "2";
      const quantity = "2";
      const productName = "iPhone";
      const tx = await entityServiceRegistryInstance.addBusinessContractInfo(
        businessContractId,
        price,
        deliveryInterval,
        quantity,
        productName,
      );
      await expect(tx)
        .to.emit(entityServiceRegistryInstance, "BusinessContractInfoCreated")
        .withArgs(businessContractId, price, deliveryInterval, quantity, productName);
    });
    it("Should check BusinessContractInfoUpdated event is emitted ", async () => {
      const businessContractId = "1223";
      const price = "200";
      const deliveryInterval = "2";
      const quantity = "2";
      const productName = "iPhone";
      const tx = await entityServiceRegistryInstance.addBusinessContractInfo(
        businessContractId,
        price,
        deliveryInterval,
        quantity,
        productName,
      );
      await expect(tx)
        .to.emit(entityServiceRegistryInstance, "BusinessContractInfoUpdated")
        .withArgs(businessContractId, price, deliveryInterval, quantity, productName);
    });
  });
  describe("Get Business Contract info", function () {
    it("Should revert when incorrect contract Id is supplied", async () => {
      const contractId = "1223";
      const reason = "Invalid contract id";
      await expect(entityServiceRegistryInstance.getBusinessContractInfo(contractId)).to.be.revertedWith(`${reason}`);
    });

    it("Should get businessContractInfo by a contract Id", async () => {
      const contractId = "1223";
      const price = "200";
      const deliveryInterval = "2";
      const quantity = "2";
      const productName = "iPhone";
      const tx = await entityServiceRegistryInstance.getBusinessContractInfo(contractId);
      console.log({ tx });
      expect(tx.businessContractId.toString()).to.equal(contractId);
      expect(tx.price.toString()).to.equal(price);
      expect(tx.delivery_interval.toString()).to.equal(deliveryInterval);
      expect(tx.quantity.toString()).to.equal(quantity);
      expect(tx.product_name.toString()).to.equal(productName);
    });
  });
});
