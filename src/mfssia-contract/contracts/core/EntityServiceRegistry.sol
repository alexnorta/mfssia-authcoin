//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

/// @title EntityServiceRegistry Contract
/// @author MFSSIA Authcoin
/// @notice This is used to record entities such as business contract, gateway and security lincense for either service provider or consumer

contract EntityServiceRegistry is Ownable {
    /*************** State attributes ***************/
    // An address type variable is used to store ethereum accounts.
    address contractOwner;

    // struct to hold the data in storage database
    struct BusinessContractInfo {
        uint256 businessContractId;
        uint256 price;
        uint256 quantity;
        uint256 delivery_interval;
        string product_name;
        bool isExist;
    }

    /// @notice Mapping of businessContractId to BusinessContractInfo record
    mapping(uint256 => BusinessContractInfo) private businessContractInfoTable;


    /*************** Emitted Events *****************/
    // when a business contract info is created
    // NewRequest that will be listened by the off-chain oracles.
    event BusinessContractInfoCreated(
        uint256 indexed businessContractId,
        uint256 price,
        uint256 delivery_interval,
        uint256 quantity,
        string indexed product_name
    );
    // when a business contract info is updated
    event BusinessContractInfoUpdated(
        uint256 indexed businessContractId,
        uint256 price,
        uint256 quantity,
        uint256 delivery_interval,
        string indexed product_name
    );

    /*********************** Modifiers *******************/
    modifier onlyNotStoredContractId(uint256 _businessContractId) {
        require(!isContractIdExist(_businessContractId), "Business Contract ID already exist exist");
        _;
    }

    modifier onlyBusinessFieldValueNotEmpty(
        uint256 _businessContractId,
        uint256 _price,
        uint256 _quantity,
        uint256 _delivery_interval,
        string memory _product_name
    ) {
        require(_businessContractId >= 0, "Contract Id cannot be left empty");
        require(_price >= 0, "Price cannot be left empty");
        require(_quantity >= 0, "Quantity cannot be left empty");
        require(_delivery_interval > 0, "Delivery date cannot be left empty");
        require(getStringLength(_product_name) != 0, "Product name cannot be left empty");
        _;
    }

    /**
     * Contract initialization.
     *
     * The `constructor` is executed only once when the contract is created.
     */
    constructor() {
        contractOwner = msg.sender;
    }

    /*********************** External methods *******************/

    /// @notice This as a new business contract info to the blockchain
    /// @param _businessContractId unique id to identify each business contract
    /// @param _price price of a defined for a particlur product name
    /// @param _delivery_interval delivery date for a particlur product name
    /// @param _quantity quantity of product that is required
    /// @param _product_name the product name
    function addBusinessContractInfo(
        uint256 _businessContractId,
        uint256 _price,
        uint256 _delivery_interval,
        uint256 _quantity,
        string memory _product_name
    )
        public
        onlyOwner
        onlyBusinessFieldValueNotEmpty(_businessContractId, _price, _quantity, _delivery_interval, _product_name)
    {
        require(msg.sender != address(0x0), "Ensure owner address exist");
        bool exists = isContractIdExist(_businessContractId);
        if(exists == true){
            updateBusinessContractInfo(_businessContractId, _price, _quantity, _delivery_interval, _product_name);
        } else {
            BusinessContractInfo memory businessContractInfo = BusinessContractInfo({
            businessContractId: _businessContractId,
            price: _price,
            delivery_interval: _delivery_interval,
            quantity: _quantity,
            product_name: _product_name,
            isExist: true
        });
        businessContractInfoTable[_businessContractId] = businessContractInfo;
         emit BusinessContractInfoCreated(_businessContractId, _price, _delivery_interval, _quantity, _product_name);
        }
       

    }

    // // this update an already existing business contract by id
    // updateBusinessContract

    /// @notice gets the a particular contract info by their id
    function getBusinessContractInfo(uint256 _businessContractId)
        public
        view
        returns (
            uint256 businessContractId,
            uint256 price,
            uint256 quantity,
            uint256 delivery_interval,
            string memory product_name,
            bool isExist
        )
    {
        require(businessContractInfoTable[_businessContractId].isExist == true, "Invalid contract id");
        return (
            businessContractInfoTable[_businessContractId].businessContractId,
            businessContractInfoTable[_businessContractId].price,
            businessContractInfoTable[_businessContractId].quantity,
            businessContractInfoTable[_businessContractId].delivery_interval,
            businessContractInfoTable[_businessContractId].product_name,
            businessContractInfoTable[_businessContractId].isExist
        );
    }

    /// @notice This update a particulr contract info by id
    function updateBusinessContractInfo(
        uint256 _businessContractId,
        uint256 _price,
        uint256 _quantity,
        uint256 _delivery_interval,
        string memory _product_name
    ) internal returns (bool success) {
        businessContractInfoTable[_businessContractId].businessContractId = _businessContractId;
        businessContractInfoTable[_businessContractId].price = _price;
        businessContractInfoTable[_businessContractId].quantity = _quantity;
        businessContractInfoTable[_businessContractId].delivery_interval = _delivery_interval;
        businessContractInfoTable[_businessContractId].product_name = _product_name;
        businessContractInfoTable[_businessContractId].isExist = true;

        emit BusinessContractInfoUpdated(_businessContractId, _price, _quantity, _delivery_interval, _product_name);

        return true;
    }

    /// @notice can be used by a client contract to ensure that they've connected to this contract interface successfully
    /// @return true, unconditionally
    function testConnection() public pure returns (bool) {
        return true;
    }

    function isContractIdExist(uint256 _businessContractId) internal view returns (bool) {
        console.log('contractId from code', _businessContractId);
        return businessContractInfoTable[_businessContractId].isExist;
    }

    function getStringLength(string memory _str) internal pure returns (uint256) {
        uint256 len = bytes(_str).length;

        return len;
    }
}
