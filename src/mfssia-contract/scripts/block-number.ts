/* eslint-disable node/no-unpublished-import */
import { task } from "hardhat/config";

task("block-number", "Prints the current block number", async (_: any, { ethers }: any) => {
  await ethers.provider.getBlockNumber().then((blockNumber: string) => {
    console.log("Current block number: " + blockNumber);
  });
});
