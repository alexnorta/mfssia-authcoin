/* eslint-disable no-process-exit */
import { ethers, upgrades } from "hardhat";

const CONTRACT_ADDRESS: string = "0x0c68399640edC7d5fABEd08e8B0af7A88F43ED3d";

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  // Upgrading
  const entityServiceRegistryFactoryV2 = await ethers.getContractFactory("EntityServiceRegistry");
  const upgraded = await upgrades.upgradeProxy(CONTRACT_ADDRESS, entityServiceRegistryFactoryV2);
  console.log("EntityServiceRegistry upgraded:", upgraded);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
