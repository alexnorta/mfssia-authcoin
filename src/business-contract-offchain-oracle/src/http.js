const axiosInstance = require('axios')

const {
    responseErrorMessage,
    responseMessage,
    responseData,
  } = require('./response')
  

const getSystem_1_Hash = async (contractId) => {
  const system_1_Url = `http://ec2-44-213-31-19.compute-1.amazonaws.com:4000/v1/api/business-contract/${contractId}/system_1-hash`
    try {
      const responseBody = await axiosInstance.get(system_1_Url)
      if (responseBody.status > 299) {
        return responseMessage(responseBody)
      }
      return responseData(responseBody)
    } catch (error) {
      return responseErrorMessage(error)
    }
  }

  const getSystem_2_Hash = async (contractId) => {
    const system_2_Url = `http://ec2-3-232-91-206.compute-1.amazonaws.com:4000/v1/api/business-contract/${contractId}/system_2-hash`
    try {
      const responseBody = await axiosInstance.get(system_2_Url)
      if (responseBody.status > 299) {
        return responseMessage(responseBody)
      }
      return responseData(responseBody)
    } catch (error) {
      return responseErrorMessage(error)
    }
  }
  
  const getDGKHash = async (contractId) => {
    const DKGUrl = `https://api.mfssia.tk/api/consensus/contractHash/${contractId}`
    try {
      const responseBody = await axiosInstance.get(DKGUrl)
      if (responseBody.status > 299) {
        return responseMessage(responseBody)
      }
      return {status: 200, dkgHash: responseBody.data}

    } catch (error) {
      return responseErrorMessage(error)
    }
  }
  
  const getGatewayFromDKG = async (contractId) => {
    const gatewayDKGUrl =  `https://api.mfssia.tk/api/consensus/gatewayConsensus/${contractId}`
    try {
      const responseBody = await axiosInstance.get(gatewayDKGUrl)
      if (responseBody.status > 299) {
        return responseMessage(responseBody)
      }
      return responseBody.data

    } catch (error) {
      return responseErrorMessage(error)
    }
  }


  const getSecurityLicenseFromDKG = async (ownerId1, ownerId2) => {
    const securityLicenseDKGUrl = `https://api.mfssia.tk/api/consensus/securityLicenseConsenus/owner1/${ownerId1}/owner2/${ownerId2}`
     try {
       const responseBody = await axiosInstance.get(securityLicenseDKGUrl)
       if (responseBody.status > 299) {
         return responseMessage(responseBody)
       }
       return responseBody.data
 
     } catch (error) {
       return responseErrorMessage(error)
     }
   }
  
  module.exports = {
    getSystem_1_Hash,
    getSystem_2_Hash,
    getDGKHash,
    getGatewayFromDKG,
    getSecurityLicenseFromDKG
  }