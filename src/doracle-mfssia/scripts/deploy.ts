// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
import { ethers } from "hardhat";

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');
  const [deployer] = await ethers.getSigners();
  console.log("Deploying contracts with the account:", deployer.address);
  // We get the contract to deploy
  const mFSSIAOracleConsumerFactory = await ethers.getContractFactory(
    "MFSSIAOracleConsumer"
  );
  // const IexecHubInstance = await IexecHub.deployed();

  const mFSSIAOracleConsumerFactoryInstance =
    // await mFSSIAOracleConsumerFactory.deploy(IexecHubInstance.address);
    await mFSSIAOracleConsumerFactory.deploy();

  await mFSSIAOracleConsumerFactoryInstance.deployed();
  console.log(
    "MFSSIAOracleConsumer deployed to:",
    mFSSIAOracleConsumerFactoryInstance.address
  );
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.trace(error);
  process.exitCode = 1;
});

// MFSSIAOracleConsumer deployed to: 0x790aF09c0433E957CeBB2C8E6a4EC490d1d21bE9
