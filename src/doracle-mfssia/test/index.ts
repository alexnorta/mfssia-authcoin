/* eslint-disable node/no-missing-import */
/* eslint-disable camelcase */
import { ethers, waffle } from "hardhat";
import chai from "chai";

import MFSSIAOracleConsumerArtifact from "../artifacts/contracts/MFSSIAOracleConsumer.sol/MFSSIAOracleConsumer.json";
import { MFSSIAOracleConsumer } from "../typechain/types/MFSSIAOracleConsumer";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

const { deployContract } = waffle;
const { expect } = chai;

describe("MFSSIAOracleConsumer Contract", () => {
  let owner: SignerWithAddress;
  let mFSSIAOracleConsumerInstance: MFSSIAOracleConsumer;

  beforeEach(async () => {
    [owner] = await ethers.getSigners();

    mFSSIAOracleConsumerInstance = (await deployContract(
      owner,
      MFSSIAOracleConsumerArtifact
    )) as MFSSIAOracleConsumer;
  });

  describe("Receive Off-chain oracle result and update", () => {
    it("Should receive task id and update contract state", async () => {
      const options = { gasPrice: 1000000000, gasLimit: 6700000 };
      const taskId =
        "0xbbf7958bfa420d310cca43a094ddc1efe2371aefba62f51613fe9ab14e0522aa";
      const transaction = await mFSSIAOracleConsumerInstance.receiveResult(
        taskId,
        "0x",
        options
      );
      expect(transaction.from).to.equal(owner.address);
    });

    it("Should check NewChallengeRequest event is emitted ", async () => {
      const options = { gasPrice: 10000000000, gasLimit: 6700000 };
      const taskId =
        "0xbbf7958bfa420d310cca43a094ddc1efe2371aefba62f51613fe9ab14e0522aa";
      const oracleCallID = "";
      const contractId = "1234";
      const connectionId = 2;
      const business_contract = true;
      const security_license = true;
      const gateway = true;
      const connection_state = "Green";
      const tx = await mFSSIAOracleConsumerInstance.receiveResult(
        taskId,
        "0x",
        options
      );
      await expect(tx)
        .to.emit(mFSSIAOracleConsumerInstance, "NewChallengeRequest")
        .withArgs(
          oracleCallID,
          contractId,
          connectionId,
          business_contract,
          security_license,
          gateway,
          connection_state
        );
    });
  });
});
