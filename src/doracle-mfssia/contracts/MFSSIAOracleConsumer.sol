//SPDX-License-Identifier: Unlicense
pragma solidity ^0.6.12;
pragma experimental ABIEncoderV2;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@iexec/solidity/contracts/ERC1154/IERC1154.sol";
import "@iexec/doracle/contracts/IexecDoracle.sol";
import "hardhat/console.sol";

/// @title MFSSIAOracleConsumer Contract
/// @author MFSSIA Authcoin
/// @notice This is used to consume the result of the off-chain oracle

contract MFSSIAOracleConsumer is IexecDoracle, Ownable, IOracleConsumer {
    // Data storage
    uint256 private connectionID;

    struct ResultAggregatorMetadata {
        bytes32 oracleCallID;
        uint256 contractId;
        uint256 connectionId;
        bool business_contract;
        bool security_lincense;
        bool gateway;
        string connection_state;
    }

    /// @notice Mapping of businessContractId to resultAggregatorTable record
    mapping(uint256 => ResultAggregatorMetadata) public resultAggregatorTable;

 /// @notice Event to track new challenge request oracle result
    event NewChallengeRequest(
        bytes32 indexed oracleCallID,
        uint256 indexed contractId,
        uint256 connectionId,
        bool business_contract,
        bool security_lincense,
        bool gateway,
        string connection_state
    );

    /// @notice  Use _iexecHubAddr to force use of custom iexechub, leave 0x0 for autodetect
    constructor() public IexecDoracle(address(0)) {}

    function updateEnv(
        address _authorizedApp,
        address _authorizedDataset,
        address _authorizedWorkerpool,
        bytes32 _requiredtag,
        uint256 _requiredtrust
    ) public onlyOwner {
        _iexecDoracleUpdateSettings(
            _authorizedApp,
            _authorizedDataset,
            _authorizedWorkerpool,
            _requiredtag,
            _requiredtrust
        );
    }

   
   /// @notice This is used to update this contract with the result decoded from the off-chain oracle
    // ERC1154 - Callback processing
    function receiveResult(bytes32 _callID, bytes calldata) external override {
        // Parse results
        (
            uint256 contractId,
            bool business_contract,
            bool securityLicense,
            bool gateway,
            string memory connection
        ) = abi.decode(
                _iexecDoracleGetVerifiedResult(_callID),
                (uint256, bool, bool, bool, string)
            );

        // Process results
        // bytes32 id = keccak256(bytes(connection_state));
        resultAggregatorTable[contractId].oracleCallID = _callID;
        resultAggregatorTable[contractId].contractId = contractId;
        resultAggregatorTable[contractId].connectionId = getConnectionId();
        resultAggregatorTable[contractId].business_contract = business_contract;
        resultAggregatorTable[contractId].security_lincense = securityLicense;
        resultAggregatorTable[contractId].gateway = gateway;
        resultAggregatorTable[contractId].connection_state = connection;

        emit NewChallengeRequest(
            _callID,
            contractId,
            connectionID,
            business_contract,
            securityLicense,
            gateway,
            connection
        );

    }

    /// @notice gets the a particular contract info by their id
    function getResultByContractId(uint256 _businessContractId)
        public
        view
        returns (
            bytes32 oracleCallID,
            uint256 contractId,
            uint256 connectionId,
            bool business_contract,
            bool security_lincense,
            bool gateway,
            string memory connection_state
        )
    {
        return (
            resultAggregatorTable[_businessContractId].oracleCallID,
            resultAggregatorTable[_businessContractId].contractId,
            resultAggregatorTable[_businessContractId].connectionId,
            resultAggregatorTable[_businessContractId].business_contract,
            resultAggregatorTable[_businessContractId].security_lincense,
            resultAggregatorTable[_businessContractId].gateway,
            resultAggregatorTable[_businessContractId].connection_state
        );
    }

    function getConnectionId() internal returns (uint256) {
        return connectionID++;
    }

    function testConnection() public pure returns (bool) {
        return true;
    }
}
