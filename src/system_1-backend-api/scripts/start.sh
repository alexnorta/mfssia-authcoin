#!/bin/bash
docker build -t mfssia-api .
docker run -it -p 3000:3000 mfssia-api
docker run -it -p 5000:5000 --env-file .env mfssia-api
