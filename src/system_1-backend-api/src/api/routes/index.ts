import { Router } from 'express';

import businessContractRoutes from '../v1/business-contract';

import getLogger from '../../logger';

const logger = getLogger.router;
const apiRouter = Router();
logger.info('💻 SETUP - Installing Routes...');

apiRouter.get('/', (req, res) => {
  res.status(200).json('Business Contract Server is back!');
});

apiRouter.use('/business-contract', businessContractRoutes);

export default apiRouter;
