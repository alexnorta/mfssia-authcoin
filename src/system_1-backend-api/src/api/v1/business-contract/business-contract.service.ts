/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { generateSha256Hash } from '../../../helpers/hash';
import { getContractInstance } from '../../../helpers/network';
import serviceAbi from './business-contract-abi.json';
import { IBusinessContract } from './business-contract.interface';

const contractInstance = getContractInstance(serviceAbi);

const hashBusinessContractInfoByContractId = async (contractId) => {
  const foundBusinessContract = await contractInstance.getBusinessContractInfo(contractId);
  const prepareResult: Omit<IBusinessContract, 'contractId'> = {
    price: foundBusinessContract[1].toString(),
    quantity: foundBusinessContract[2].toString(),
    delivery_date: foundBusinessContract[3].toString(),
    product_name: foundBusinessContract[4].toString(),
  };
  const hashedResult = generateSha256Hash(
    prepareResult.price,
    prepareResult.quantity,
    prepareResult.delivery_date,
    prepareResult.product_name,
  );

  return { hash: hashedResult };
};

export default {
  hashBusinessContractInfoByContractId,
};
