import Joi from '@hapi/joi';

export const createBusinessContractInfoSchema = Joi.object().keys({
  contractId: Joi.string().trim().required(),
  price: Joi.string().trim().required(),
  delivery_date: Joi.string().trim().required(),
  quantity: Joi.string().trim().required(),
  product_name: Joi.string().trim().required(),
});
