/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Request, Response } from 'express';
import BusinessContractInfoService from './business-contract.service';
import { onCreateResponse, onFailure } from '../../../response-manager';

export const generateBusinessContractHashValue = async (req: Request, res: Response) => {
  try {
    const { contractId } = req.params;
    const foundBusinessContractByContractId: any =
      await BusinessContractInfoService.hashBusinessContractInfoByContractId(contractId);
    if (!foundBusinessContractByContractId) {
      onCreateResponse(res, '', 401, 'Invalid parameters passed', false);
    } else {
      onCreateResponse(
        res,
        foundBusinessContractByContractId,
        200,
        'Business contract hash generated successfully',
        true,
      );
    }
  } catch (error) {
    onFailure(res, error, 'Error occurred while generating a hash for business contract info');
  }
};

export default {
  generateBusinessContractHashValue,
};
