import { Router } from 'express';

import BusinessContractController from './business-contract.controller';

const businessContractInfoRoutes = Router();

businessContractInfoRoutes
  .route('/:contractId/system_1-hash')
  .get(BusinessContractController.generateBusinessContractHashValue);

export default businessContractInfoRoutes;
