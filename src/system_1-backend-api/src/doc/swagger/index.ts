/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import express from 'express';
import swaggerUi from 'swagger-ui-express';
import basicAuth from 'express-basic-auth';

import swaggerDocument from './swagger.json';
import getLogger from '../../logger';

const logger = getLogger.swagger;

const options = {
  explorer: true,
  swaggerOptions: {
    docExpansion: 'none',
  },
  customSiteTitle: 'MFSSIA API Documentation',
  customCss: `
  .topbar-wrapper img {content:url(https://media-exp1.licdn.com/dms/image/C4D0BAQG2tbXXC-FAaw/company-logo_200_200/0/1620571933436?e=1643241600&v=beta&t=VhvEAp_NOUOCnUQG3TyETGC4O2Kx4Goimzz8J5HLb1Q); width:50px; height:auto;}
  .swagger-ui .topbar { background-color: #1971f5; border-bottom: 20px solid #5dc6d1; },
      `,
  // customFavIcon: './favicon.ico',
};

export const initSwagger = (app) => {
  logger.info('Setting up swagger doc tool');
  app.use('/favicon.ico', express.static('./favicon.ico'));
  app.use(
    '/api-docs',
    basicAuth({
      authorizer: myAuthorizer,
      challenge: true,
    }),
    swaggerUi.serve,
    swaggerUi.setup(swaggerDocument, options),
  );
};

function myAuthorizer(username: string, password: string) {
  const userMatches = basicAuth.safeCompare(username, 'mfssia');
  const passwordMatches = basicAuth.safeCompare(password, 'admin');

  return userMatches && passwordMatches;
}
