/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import hpp from 'hpp';
import helmet from 'helmet';
import { v4 as uuidv4 } from 'uuid';
import featurePolicy from 'feature-policy';

function generateNonce(req, res, next) {
  const rhyphen = /-/g;
  res.locals.nonce = uuidv4().replace(rhyphen, ``);
  next();
}

function getNonce(req, res) {
  return `'nonce-${res.locals.nonce}'`;
}

function getDirectives() {
  const self = `'self'`;
  const unsafeInline = `'unsafe-inline'`;
  const authorityIsSecure = true;
  const scripts = [`https://www.google-analytics.com/`];
  const styles = [`https://fonts.googleapis.com/`, `www.google-analytics.com`];
  const frames = [`https://www.youtube.com/`];
  const fonts = [`https:`, `data:`];
  const images = [`https:`, `data:`];
  const connect = [`https`, `https://maps.googleapis.com/`];

  return {
    defaultSrc: [self],
    scriptSrc: [self, getNonce, ...scripts],
    styleSrc: [self, unsafeInline, ...styles],
    fontSrc: [self, ...fonts],
    frameSrc: [self, ...frames],
    connectSrc: [self, ...connect],
    imgSrc: [self, ...images],
    objectSrc: [self],
    childSrc: ['https:', self],
    mediaSrc: [self],
    baseUri: [self],
    // upgradeInsecureRequests: authorityIsSecure,
    // reportUri: `/api/csp/report`
  };
}

export function initializeHelmetSecurity(server, { enableNonce, enableCSP }) {
  // Don't expose any software information to hackers.
  // server.disable('x-powered-by');

  // Prevent HTTP Parameter pollution.
  server.use(hpp());

  // The xssFilter middleware sets the X-XSS-Protection header to prevent
  // reflected XSS attacks.
  // @see https://helmetjs.github.io/docs/xss-filter/
  server.use(helmet.xssFilter());

  // // Public-Key-Pins: This header increases the security of HTTPS. With this header, a specific cryptographic public key is associated with a specific web server.
  // server.use(
  //   helmet.hpkp({
  //     maxAge: 123,
  //     sha256s: ['Ab3Ef123=', 'ZyxawuV45='],
  //     reportUri: 'http://example.com',
  //     includeSubDomains: true,
  //   }),
  // );

  //HSTS effectively forces the client (browser accessing your server) to direct all traffic through HTTPS
  // Implement Strict-Transport-Security. This can help reduce the chance of Man-In-The-Middle (MITM) attacks
  // by reducing the frequency of requests being made over insecure channels.
  server.use(
    helmet.hsts({
      maxAge: 2592000, // 6 months in seconds. Must be at least 18 weeks to be approved by Google
      includeSubDomains: true, // Must be enabled to be approved by Google
      preload: true,
    }),
  );

  //  Hide X-Powered-By
  server.use(helmet.hidePoweredBy());

  // Frameguard mitigates clickjacking attacks by setting the X-Frame-Options header.
  // action `deny` will prevent anyone from putting this page in an iframe.
  // @see https://helmetjs.github.io/docs/frameguard/
  server.use(helmet.frameguard({ action: 'sameorigin' }));

  // Sets the X-Download-Options to prevent Internet Explorer from executing
  // downloads in your site’s context.
  // @see https://helmetjs.github.io/docs/ienoopen/
  server.use(helmet.ieNoOpen());

  // Don’t Sniff Mimetype middleware, noSniff, helps prevent browsers from trying
  // to guess (“sniff”) the MIME type, which can have security implications. It
  // does this by setting the X-Content-Type-Options header to nosniff.
  // @see https://helmetjs.github.io/docs/dont-sniff-mimetype/
  server.use(helmet.noSniff());

  // X-DNS-Prefetch-Control: https://github.com/helmetjs/dns-prefetch-control
  server.use(helmet.dnsPrefetchControl({ allow: false }));

  // https://github.com/helmetjs/referrer-policy
  server.use(helmet.referrerPolicy({ policy: 'same-origin' }));

  // header that allows a site to control which features and APIs can be used in the browser.
  server.use(
    featurePolicy({
      features: {
        fullscreen: ["'self'"],
        vibrate: ["'none'"],
        // payment: ['paystack.com'],
        syncXhr: ["'none'"],
      },
    }),
  );

  if (enableNonce) {
    // Attach a unique "nonce" to every response. This allows use to declare
    // inline scripts as being safe for execution against our content security policy.
    // @see https://helmetjs.github.io/docs/csp/
    server.use(generateNonce);
  }
  // Content Security Policy (CSP)
  // It can be a pain to manage these, but it's a really great habit to get in to.
  // @see https://helmetjs.github.io/docs/csp/

  if (enableCSP) {
    server.use(helmet.contentSecurityPolicy({ directives: getDirectives() }));
  }
}
