/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import express from 'express';
import compression from 'compression';
import methodOverride from 'method-override';
import expressSanitizer from 'express-sanitizer';
import xss from 'xss-clean';

import { setConstantHeaders } from './constant-headers';
import getLogger, { stream } from '../logger';
import morganMiddleware from '../logger/morgan';

const logger = getLogger.middleware;

const setGlobalMiddleware = (app) => {
  logger.info('Configuring all vendor middleware configs');
  setConstantHeaders(app); // enable CORS - Cross Origin Resource Sharing
  app.use(morganMiddleware);

  // Request body parsing middleware should be above methodOverride
  app.use(express.json({ limit: '300kb' })); // parse body params and attach them to req.body
  app.use(
    express.urlencoded({
      extended: true,
      limit: '300kb',
    }),
  );

  app.use(expressSanitizer());
  /* make sure this comes before any routes */
  app.use(xss());

  // Should be placed before express.static
  app.use(
    compression({
      filter: function (req, res) {
        return /json|text|javascript|css/.test(res.getHeader('Content-Type'));
      },
      level: 9,
    }),
  );
  app.use(methodOverride()); // lets you use HTTP verbs such as PUT or DELETE in places where the client doesn't support it
};

export default setGlobalMiddleware;
