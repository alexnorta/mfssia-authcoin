/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { v4 as uuid } from 'uuid';

const headerName = 'X-Request-Id';

export const setRequestId = async ({ req, res }, next) => {
  req.id = (req.headers[headerName.toLowerCase()] as string) || uuid();
  res.setHeader(headerName, req.id);

  return next();
};
