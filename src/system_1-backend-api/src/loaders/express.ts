/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import * as express from 'express';

import installApiEndpoints from '../api/routes';
import initializeMiddlewares from '../middlewares';
import { loadToobusy } from '../middlewares/toobusy';
import { initializeHelmetSecurity } from '../middlewares/security';
import { initializeErrorHandling } from './error';
import getLogger from '../logger';
import { initSwagger } from '../doc/index';

const logger = getLogger.gateway;

export default async ({ app }: { app: express.Application }) => {
  logger.info('💻 SETUP - Installing MFSSIA Authcoin server...');
  // Redirect http url to https
  app.set('trust proxy', true);
  app.disable('x-powered-by');
  app.use(loadToobusy);
  await initializeHelmetSecurity(app, { enableNonce: true, enableCSP: true });
  // await initializeCaching(app);
  await initializeMiddlewares(app);
  await initSwagger(app);
  app.use('/v1/api', installApiEndpoints);
  await initializeErrorHandling(app);

  // Return the express app
  return app;
};
