/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Response } from 'express';

import getLogger from '../logger';

const logger = getLogger.api;

interface IBasicResponse {
  success: boolean;
  statusCode: number;
  message: string;
  data: Record<string, string> | string;
  links: Array<Record<string, string>>;
}

const basicResponse: IBasicResponse = {
  success: false,
  statusCode: 200,
  message: '',
  data: {},
  links: [],
};

export function onFailure(res: Response, error, message) {
  let httpStatusCode = 500;
  if (error.httpStatusCode) {
    httpStatusCode = error.httpStatusCode;
  }

  if (process.env.ENVIRONMENT !== 'production') {
    logger.error('ERROR RESPONSE');
    logger.error(error);
  }

  const response: any = {
    success: false,
    code: 0,
    message: 'Unknown error',
  };

  if (error.name === 'NodeApiError') {
    Object.assign(response, error);
  }

  if (error.errorCode) {
    response.code = error.errorCode;
  }
  if (error.message) {
    response.message = `${message}: ${error.message}`;
  }
  if (error.stack && process.env.ENVIRONMENT !== 'production') {
    response.stack = error.stack;
  }

  res.status(httpStatusCode).json(response);
}

export const onCreateResponse = (
  res,
  data: Record<string, string> | string,
  statusCode,
  message: string | undefined = '',
  success,
) => {
  try {
    const response = Object.assign({}, basicResponse);
    response.success = success;
    response.statusCode = statusCode;
    response.message = message;
    response.data = data;
    logger.info('api-response-data:', response);
    return res.status(statusCode).json(response);
  } catch (error) {
    throw new Error(error);
  }
};

export const generateHATEOASLinks = async (foundModel, req) => {
  const returnModel: any = [];
  foundModel.map((element) => {
    const newModel = element;
    delete newModel.__v;
    newModel.links = {};
    newModel.links.self = {};
    newModel.links.self.href = `${req.protocol}://${req.headers.host}${req.baseUrl}/${newModel._id}`;
    newModel.links.self.type = 'application/hal+json';
    returnModel.push(newModel);
  });
  return returnModel;
};
type pageInfo = {
  currentPage: number;
  pageSize: number;
  hasNextPage: boolean;
  hasPreviousPage: boolean;
  previousPage: number;
  nextPage: number;
  lastPage: number;
  totalDocs: number;
};
export interface IPaginatedResponse<T> {
  pageInfo: pageInfo;
  nodes: T[];
}

export async function buildPaginatedResponse<T>(data, number, size, length): Promise<IPaginatedResponse<T>> {
  const totalItems = data.length;
  const pageNumber = Number.parseInt(number);
  const pageSize = Number.parseInt(size);
  return {
    pageInfo: {
      totalDocs: length,
      currentPage: pageNumber ?? 1,
      pageSize: pageSize,
      hasNextPage: pageSize * pageNumber < totalItems,
      hasPreviousPage: pageNumber > 1,
      nextPage: pageNumber + 1,
      previousPage: pageNumber - 1,
      lastPage: Math.ceil(totalItems / pageSize),
    },
    nodes: data || [],
  };
}
