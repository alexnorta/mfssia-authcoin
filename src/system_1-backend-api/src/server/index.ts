/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import http from 'http';
import express from 'express';
import { Application } from 'express';

import { appInitLoader } from '../loaders';
import config from '../config';
import getLogger from '../logger';
import { registerProcessSignals, nodeErrorHandler } from '../exceptions';
import { consumeEventFromBlockchain } from '../service/consume';

const logger = getLogger.initServer;

const app: Application = express();
const server = http.createServer(app);

export async function startServer() {
  await appInitLoader({ expressApp: app });
  server
    .listen(config.appKey.port, () =>
      logger.info(`👂 MFSSIA-AUTHCODE Api server started on port ${config.appKey.port} on (${config.appKey.env}) mode`),
    )
    .on('error', nodeErrorHandler);
}

startServer();
registerProcessSignals(server);
consumeEventFromBlockchain();

export default server;
