/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import crypto from 'crypto';

export function generateSha256Hash(price, delivery_date, quantity, product_name) {
  const strings = [`${price}`, `${delivery_date}`, `${quantity}`, `${product_name}`];
  const concatenatedResults = strings.join(';');
  console.log(concatenatedResults);
  const sanitized = concatenatedResults.replace(/(^"|"$)/g, '');
  console.log(sanitized);
  const hashResult = crypto.createHash('sha256').update(sanitized).digest('hex');
  console.log(hashResult);
  return hashResult;
}
