/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import chalk from 'chalk';

import getLogger from '../logger';
import { handleError, isTrustedError } from '.';

const logger = getLogger.shutdown;
const conductorInterval = 60;
const timeout: any = conductorInterval * 1000;

enum ExitStatus {
  Failure = 1,
  Success = 0,
}
enum ShutdownSignal {
  SIGHUP = 'SIGHUP',
  SIGINT = 'SIGINT',
  SIGQUIT = 'SIGQUIT',
  SIGILL = 'SIGILL',
  SIGTRAP = 'SIGTRAP',
  SIGABRT = 'SIGABRT',
  SIGBUS = 'SIGBUS',
  SIGFPE = 'SIGFPE',
  SIGSEGV = 'SIGSEGV',
  SIGUSR2 = 'SIGUSR2',
  SIGTERM = 'SIGTERM',
}

const listenToShutdownSignals = async (server) => {
  try {
    const exitSignals: NodeJS.Signals[] = [
      ShutdownSignal.SIGHUP,
      ShutdownSignal.SIGINT,
      ShutdownSignal.SIGQUIT,
      ShutdownSignal.SIGILL,
      ShutdownSignal.SIGTRAP,
      ShutdownSignal.SIGABRT,
      ShutdownSignal.SIGBUS,
      ShutdownSignal.SIGFPE,
      ShutdownSignal.SIGSEGV,
      ShutdownSignal.SIGUSR2,
      ShutdownSignal.SIGTERM,
    ];
    setTimeout(() => {
      exitSignals.map((exitSignal: ShutdownSignal) => {
        if (exitSignal) {
          process.on(exitSignal, async () => {
            logger.info(chalk.cyan(`Wait 15 secs for existing connection to close and then exit`));
            logger.info(chalk.white.bgRed.bold(` Process ${process.pid} received a ${exitSignal} signal`));
            try {
              logger.info(
                chalk.white.bgRed.bold(
                  'Gracefully Shutting down http server, db connection and remove all event listeners..',
                ),
              );
              logger.info(
                chalk.white.bgRed.bold(
                  `Stopping the server from accepting new connections and finishes existing connections.`,
                ),
              );
              await server.close();
              logger.info('🙉 Server is closed with success');
              process.exit(ExitStatus.Success);
            } catch (error) {
              logger.error(`App exited with error: ${error}`);
              process.exit(ExitStatus.Failure);
            }
          });
        }
      });
    }, 1500).unref();
    // }, 1).unref(); // Prevents the timeout from registering on event loop
  } catch (error) {
    logger.error(`App exited with error: ${error}`);
    process.exit(ExitStatus.Failure);
  }
};

function failure(reason, err: Error) {
  logger.info(`Unhandled ${reason}: ${err.stack}. Shutting down Conductor worker.`, err);
  try {
    handleError(err);
    clearTimeout(timeout);
  } catch (err) {
    logger.error(`Failure during Conductor worker shutdown: ${err.message}`);
  }
  if (!isTrustedError(err)) {
    process.exit(ExitStatus.Failure);
  }
}

export const registerProcessSignals = (server) => {
  listenToShutdownSignals(server);
  // to trace warning error
  process
    .on('warning', (warning) => {
      // to trace warning error
      logger.warn(warning.name); // Print the warning name
      logger.warn(warning.message); // Print the warning message
      logger.warn(warning.stack); // Print the stack trace
    })
    .on('unhandledRejection', failure.bind(null, 'promise rejection'))
    .on('uncaughtException', failure.bind(null, 'exception'))
    .on('multipleResolves', (type, promise, reason) => {
      // Displaying the error
      logger.info('Type: ', type);
      logger.info('Promsie: ', promise);
      logger.info('Reason: ', reason);
      setImmediate(() => process.exit(1));
    });
};
