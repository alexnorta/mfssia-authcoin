# System_1 backend API to interact with Polygon Network  🛡️

# Tools

- NodeJS
- Express
- Typescript
- Ether.js

## Getting Started

The easiest way to get started is to clone the repository:

# clone the repository

```
git clone https://melitus@bitbucket.org/alexnorta/mfssia-authcoin.git
```

# Change directory

```
cd mfssia-authcoin/src/system_2_backend-api```

# Install NPM dependencies

```
yarn install
```

# start the server

```
yarn run dev
```
## TO run with docker 
### Build docker image
```docker build -t system_2_backend-api .````

### Run docker image
```docker run --rm -p 4001:4001 --name system_2-api system_2_backend-api```

Note: It is recommended to install nodemon for livereloading - It watches for any changes in your node.js app and automatically restarts the server


## Business Contract Endpoint
 - Generate hash - `http://localhost:4000/v1/api/business-contract/1234/system_2-hash`
