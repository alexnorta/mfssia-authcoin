import { getContractInstance } from '../helpers/network';
import serviceAbi from '../api/v1/business-contract/business-contract-abi.json';

const contractInstance = getContractInstance(serviceAbi);

const newBusinessContractInfoCreated = (callback) => {
  try {
    contractInstance.on('BusinessContractInfoCreated', (from, to, value, event) => {
      console.log(from, to, value);
      console.log(event.blockNumber);
    });
  } catch (error) {
    console.log(error);
  }
};

const businessContractInfoUpdated = (callback) => {
  try {
    contractInstance.on('BusinessContractInfoUpdated', (from, to, value, event) => {
      console.log(from, to, value);
      console.log(event.blockNumber);
    });
  } catch (error) {
    console.log(error);
  }
};

export const consumeEventFromBlockchain = () => {
  businessContractInfoUpdated((error, result) => {
    console.log('UPDATE REQUEST DATA EVENT ON SMART CONTRACT');
    console.log('BLOCK NUMBER: ');
    console.log('  ' + result.blockNumber);
    console.log('UPDATE REQUEST DATA: ');
    console.log(result.args);
    console.log('\n');
  });

  newBusinessContractInfoCreated((error, result) => {
    console.log('NEW REQUEST DATA EVENT ON SMART CONTRACT');
    console.log('BLOCK NUMBER: ');
    console.log('  ' + result.blockNumber);
    console.log('NEW REQUEST DATA: ');
    console.log(result.args);
    console.log('\n');
  });
};
