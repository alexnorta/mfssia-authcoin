/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import crypto from 'crypto';

export function generateSha256Hash(price, delivery_date, quantity, product_name) {
  const strings = [`${price}`, `${delivery_date}`, `${quantity}`, `${product_name}`];
  const concatenatedResults = strings.join(';');
  const sanitized = concatenatedResults.replace(/(^"|"$)/g, '');
  const hashResult = crypto.createHash('sha256').update(sanitized).digest('hex');

  return hashResult;
}
