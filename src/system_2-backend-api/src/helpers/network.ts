/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { ethers } from 'ethers';

export const getContractInstance = (contractABI) => {
  const NETWORK = 'https://polygon-mumbai.g.alchemy.com/v2/mhubXm17W27tcT-Sw6Lmpmrc2Mmq3Sgb';
  const provider = ethers.providers.getDefaultProvider(NETWORK);
  const contractAddress = '0x36c176085ACAaf05da722D5A7A2fDD33f9B753AD';
  const privateKey = '38bf8d751f731a2922c7aacbed07c4d3d26893f42ca4cf4f7d5d259633f3e267';
  const wallet = new ethers.Wallet(privateKey, provider);
  const contractInstance = new ethers.Contract(contractAddress, contractABI.abi, wallet);
  return contractInstance;
};
