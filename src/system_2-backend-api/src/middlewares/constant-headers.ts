/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import cors, { CorsOptions } from 'cors';

import config from '../config';
import getLogger from '../logger';

const logger = getLogger.cors;
const whitelist = config.cors.CLIENT_ORIGIN;

// Accept requests from our client
export const getAccessControlAllowOriginOptions: CorsOptions = {
  origin: whitelist,
  credentials: config.cors.credentials,
  optionsSuccessStatus: config.cors.optionsSuccessStatus,
  methods: config.cors.methods,
};

export const setConstantHeaders = (app) => {
  logger.info('Configuring cors');
  app.use(cors());
  app.options('*', cors()); // Opt in to Browser pre-flight checks.    This is important.
};
