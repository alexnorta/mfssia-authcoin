import path from 'path';
import * as dotenv from 'dotenv';

const envPath = path.join(
  process.cwd(),
  `.env${!process.env.NODE_ENV || process.env.NODE_ENV === 'development' ? '' : `.${process.env.NODE_ENV}`}`,
);

const loadenv = () =>
  dotenv.config({
    path: envPath,
  });

loadenv();

export default {
  appKey: {
    port: process.env.PORT || 4001,
    env: process.env.NODE_ENV || 'production',
  },
  cors: {
    CLIENT_ORIGIN: ['https://127.0.0.1:4200'],
    credentials: true,
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
    methods: ['POST', 'GET', 'PUT', 'DELETE', 'PATCH'],
  },
};
