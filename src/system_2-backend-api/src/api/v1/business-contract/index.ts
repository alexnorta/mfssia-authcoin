import { Router } from 'express';

import BusinessContractController from './business-contract.controller';

const businessContractInfoRoutes = Router();

businessContractInfoRoutes
  .route('/:contractId/system_2-hash')
  .get(BusinessContractController.generateBusinessContractHashValue);

export default businessContractInfoRoutes;
