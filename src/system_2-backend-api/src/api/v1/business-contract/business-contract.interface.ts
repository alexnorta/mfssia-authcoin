export interface IBusinessContract {
  contractId: string;
  price: string;
  delivery_date: string;
  quantity: string;
  product_name: string;
}
