/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import expressLoader from './express';
import getLogger from '../logger';

const logger = getLogger.initServer;

export const appInitLoader = async ({ expressApp }) => {
  await expressLoader({ app: expressApp });
  logger.info('👌 Express Initialized');
};
