An iExec DOracle needs two parts: Off-chain dockerized application, and on-chain smart contract.
1. Dockerized application (off-chain) You need to write a logic that does the following steps:
- Fetch the data from the API
- store the hash of the result in the determinism.iexec file
- store the encoded result (using ABI.encode) in the callback.iexec file
Then, push you docker image on Dockerhub, and deploy it on iExec using the SDK.
2. Smart contract (on-chain)
- Edit the provided template by filling your app ID
- Use truffle to deploy the smart contract


- After consensus on the deterministic result has been achieved, the scheduler will call the function named ‘receiveResult’ from the originating contract instance, which in this case is the ongoing purchase contract, and the function will retrieve the callback result from the executed Task to confirm if the consensus was that the client’s data was valid. The ‘receiveResult’ also stores the hash of both the shipping address and payment method delivered by the callback result of the task, and sets the new state of the contract to ‘VALIDATED’.