import { ethers } from 'ethers';
import ContractAbi from '../abis/MFSSIAOracleConsumer.json';

const NETWORK ="https://bellecour.iex.ec";
const contractAddress = '0xFC3b209CdA5F8395DEc3ADEFd3Da327A4B7948e2'; // Bellecour from onchain


export const getContractInstance = (contractABI) => {
  const provider = ethers.providers.getDefaultProvider(NETWORK);
  const privateKey = 'a5a58bad5f870fc7864171c568e5ab5612d0ec3d39ca45a89137cff07c1cf6e3';

  const wallet = new ethers.Wallet(privateKey, provider);
  const contractInstance = new ethers.Contract(contractAddress, contractABI.abi, wallet);
  return contractInstance;
};

export const getSigner = async () => {
  const provider = ethers.providers.getDefaultProvider(NETWORK);
  const privateKey = 'a5a58bad5f870fc7864171c568e5ab5612d0ec3d39ca45a89137cff07c1cf6e3';

  const signer = new ethers.Wallet(privateKey, provider);
  return signer
}


export const getContractInstanceFromSigner = async (contractABI) => {
 const signer = await getSigner()
  const contractInstance = new ethers.Contract(contractAddress, contractABI.abi, signer);
  return contractInstance;
};

const ContractAddress = process.env.REACT_APP_CONTRACT_ADDRESS;

export const getContractByProvider = async (provider) => {
  try {
    const { abi } = ContractAbi;
    const contract = new ethers.Contract(ContractAddress, abi, provider);
    return contract;
  } catch (err) {
    throw err;
  }
};
export const getContractBySigner = async (signer) => {
  try {
    const { abi } = ContractAbi;
    const contract = new ethers.Contract(ContractAddress, abi, signer);
    return contract;
  } catch (err) {
    throw err;
  }
};

