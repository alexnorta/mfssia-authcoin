
import * as utils from '../utils';

const checkStorage = async (iexec) => {
  try {
    const isStorageInitialized = await iexec.storage.checkStorageTokenExists(
      await iexec.wallet.getAddress()
    );
   return isStorageInitialized
  } catch (error) {
    // storageCheckError.innerText = error.message;
  }
};

export const initStorage = async (iexec) => {
  try {
   
    const storageToken = await iexec.storage.defaultStorageLogin();
    const result =  await iexec.storage.pushStorageToken(storageToken, { forceUpdate: true });
    checkStorage(iexec);
    console.log('result storage', result)
    return result
  } catch (error) {
    console.trace(error)
  } 
};

export const buyComputation = async (iexec, contractId,toast) => {
    try {
      const appAddress = '0x46c185f26f8169b3136F23D93C3e0E6154B2c1de'; // from offchain oracle server
      const category = '0';
      const params = contractId;
      toast.success("Fetching app order book!", {
        position: "top-left",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      await utils.sleep(7000);
      const { orders: appOrders } = await iexec.orderbook.fetchAppOrderbook(
        appAddress
      );
      console.log({appOrders})
      const appOrder = appOrders && appOrders[0] && appOrders[0].order;
      if (!appOrder) throw Error(`no apporder found for app ${appAddress}`);
      const {
        orders: workerpoolOrders
      } = await iexec.orderbook.fetchWorkerpoolOrderbook({ category });
      console.log({workerpoolOrders})
      const workerpoolOrder =
        workerpoolOrders && workerpoolOrders[0] && workerpoolOrders[0].order;
      if (!workerpoolOrder)
        throw Error(`no workerpoolorder found for category ${category}`);
  
      const userAddress = await iexec.wallet.getAddress();
      console.log({userAddress})
      toast.success("Creating an order request!", {
        position: "top-left",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      await utils.sleep(2000);
      const requestOrderToSign = await iexec.order.createRequestorder({
        app: appAddress,
        appmaxprice: appOrder.appprice,
        workerpoolmaxprice: workerpoolOrder.workerpoolprice,
        requester: userAddress,
        volume: 1,
        params: {iexec_args: params} || "",
        callback:"0x8F72b1Fc2B56728d24AbADA6134A53c9bF298978",
        category: category
      });
      console.log({requestOrderToSign})
      await utils.sleep(2000);
      toast.success("Sign the request order with the desired data", {
        position: "top-left",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      const requestOrder = await iexec.order.signRequestorder(requestOrderToSign);
      console.log({requestOrder})
      await utils.sleep(2000);
      toast.success("Waiting for the POCO matchmaking to take place to get a deal ", {
        position: "top-left",
        autoClose: 8000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      const res = await iexec.order.matchOrders({
        apporder: appOrder,
        requestorder: requestOrder,
        workerpoolorder: workerpoolOrder
      });
      console.log({res})
      return res.dealid
     
    } catch (error) {
      console.trace(error)
    } 
  };

  export const showOrderbook = async (iexec) => {
    try {     
      const appAddress = '0xbBA0d689BA4809f688a2e30cEB6966e3213bf547';
        const {
            appOrders
        } = await iexec.orderbook.fetchAppOrderbook(appAddress);
        return JSON.stringify(appOrders, null, 2);
    } catch (error) {
        // appOrderbookShowError.innerText = error;
    }
};

export const showPreviousDeals =  async (iexec) => {
    try {
        const userAddress = await iexec.wallet.getAddress();
        const deals = await iexec.deal.fetchRequesterDeals(userAddress);
        return JSON.stringify(deals, null, 2)
    } catch (error) {
        // previousDealsError.innerText = error;
    } 
};

export const showDeal = async (iexec, dealId) => {
    try {
        const dealid = dealId;
        const deal = await iexec.deal.show(dealid);
        console.log('deaf from show deal', deal)
        return deal
    } catch (error) {
        // resultsShowDealError.innerText = error;
    } 
};

export const showTask = async (iexec, taskId) =>  {
    try {
       
        const taskid = taskId;
        const task = await iexec.task.show(taskid);
        return task;
    } catch (error) {
        // resultsShowTaskError.innerText = error;
    } 
};

export const getTaskIdFromDealId = async (iexec, dealId) =>  {
    try {
       
        const dealid = dealId;
        const res = await iexec.deal.computeTaskId(dealid);
        console.log('taskfromdeal', res)
        return JSON.stringify(res, null, 2);
    } catch (error) {
        // resultsShowTaskError.innerText = error;
    } 
};


export const dowloadResults = async (iexec, taskid) =>{
  console.log('taskid download', taskid)
  try {
    const res = await iexec.task.fetchResults(taskid);
    console.log('download resutl', res)
    const file = await res.blob();
    const fileName = `${taskid}.zip`;
    if (window.navigator.msSaveOrOpenBlob)
      window.navigator.msSaveOrOpenBlob(file, fileName);
    else {
      const a = document.createElement("a");
      const url = URL.createObjectURL(file);
      a.href = url;
      a.download = fileName;
      document.body.appendChild(a);
      a.click();
      setTimeout(() => {
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);
      }, 0);
    }
  } catch (error) {
    throw Error(error)
    // resultsDownloadError.innerText = error;
  } 
};

const showOutput = iexec => async taskId => {
  try {
    if (!taskId) {
      throw new Error('No task id')
    }

    const res = await iexec.task.fetchResults(taskId, {
      ipfsGatewayURL: "https://ipfs.iex.ec"
    });
    const file = await res.blob();

    function blob2data(blob) {
      const fd = new FormData();
      fd.set("a", blob);
      return fd.get("a");
    }
    const rawData = blob2data(file);

    const JSZip = require("jszip");
    const zip = new JSZip();
    const data = await zip.loadAsync(rawData);

    const stdFile = data.file("stdout.txt");
    const stdText = await stdFile.async("string");

    document.getElementById('build-output').innerText = stdText;
    window.scrollTo(document.getElementById('build-output'));
  } catch (e) {
    document.getElementById('build-output').innerText = e;
  }
};