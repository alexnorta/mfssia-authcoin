import { IExec } from "iexec";

export const loadIExec = async () => {
  try {
    let ethProvider;

    if (window.ethereum) {
      console.log("using default provider");
      ethProvider = window.ethereum;
    }

    let networkmap = new Map([
      [1, "Ethereum Mainnet"],
      [5, "Goerli Testnet"],
      [134, "Bellecour Sidechain"],
      [133, "iExec Test Sidechain"],
    ]);

    await ethProvider.enable();

    const { result } = await new Promise((resolve, reject) =>
      ethProvider.sendAsync(
        {
          jsonrpc: "2.0",
          method: "net_version",
          params: [],
        },
        (err, res) => {
          if (!err) resolve(res);
          reject(Error(`Failed to get network version from provider: ${err}`));
        }
      )
    );
    const networkVersion = result;

    if (networkmap.get(parseInt(networkVersion)) == undefined) {
      const error = `Unsupported network ${networkVersion}, please switch to Goerli or Bellecour`;
      // storageInitButton.innerText = "invalid network";
      // networkOutput.innerText = error;
      throw Error(error);
    }

    // networkOutput.innerText = networkmap.get(parseInt(networkVersion));

    const iexec = new IExec({
      ethProvider,
      chainId: networkVersion,
    });

    return iexec;
  } catch (e) {
    console.error(e.message);
  }
};
