import React, { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import Spinner from "react-bootstrap/Spinner";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast, Slide } from "react-toastify";

import "./App.css";
import { loadIExec } from "./lib/iexec";
import {
  buyComputation,
  initStorage,
  showDeal,
} from "./lib/operations";
import MfssiaConsumerContractAbi from "./abis/MFSSIAOracleConsumer.json";
import { getContractInstance } from "./lib/network";
import * as utils from "./utils";

const contractInstance = getContractInstance(MfssiaConsumerContractAbi);

export default function App() {
  const [inputContractId, setInputContractId] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [ipfsStorage, setIpfsStorage] = useState("");

  const [offchainResultInfo, setOffchainResultInfo] = useState({
    contractId: "-",
    oracleCallID: "-",
    connectionId: "-",
    business_contract: "-",
    security_lincense: "-",
    gateway: "-",
    connection_state: "-",
  });

  const onNewChallengeSetEvent = async (...args) => {
    const result = {
      oracleCallID: args[0].toString(),
      contractId: args[1].toString(),
      connectionId: args[2].toString(),
      business_contract: args[3].toString(),
      security_lincense: args[4].toString(),
      gateway: args[5].toString(),
      connection_state: args[6].toString(),
    };
    setOffchainResultInfo({
      contractId: result.contractId,
      oracleCallID: result.oracleCallID,
      connectionId: result.connectionId,
      business_contract: result.business_contract,
      security_lincense: result.security_lincense,
      gateway: result.gateway,
      connection_state: result.connection_state,
    });
  };

  function displayConnectionState(state){
    if(state === "red"){
      return <span style={{ color: "red", fontWeight: "800", fontSize: "20px" }} >Red</span>
    }else if(state === "green"){
      return "Green"
    }
    return '-'
 }

  const reset = () => {
    setOffchainResultInfo({
      contractId: "-",
      oracleCallID: "-",
      connectionId: "-",
      business_contract: "-",
      security_lincense: "-",
      gateway: "-",
      connection_state: "-",
    });
  };
  
  useEffect(() => {
    reset()
  }, []);

  useEffect(() => {
    contractInstance.on("NewChallengeRequest", onNewChallengeSetEvent);
    return () => {
      contractInstance.off("NewChallengeRequest", onNewChallengeSetEvent);
    };
  }, []);

  useEffect(() => {
    (async () => {
      setIsLoading(false);
    })();
  }, []);

  const receiveResultFromOffChainOracle = async (taskId) => {
    var options = { gasPrice: 1000000000, gasLimit: 1000000 };
    try {
      await contractInstance.receiveResult(taskId, "0x", options);
    } catch (e) {
      console.error(e);
    }
  };

  const handleRemoteStorage = async (e) => {
    e.preventDefault();
    try {
      const iexec = await loadIExec();
      const response = await initStorage(iexec);
      if (response.isPushed === true) {
        setIpfsStorage("Initialized");
      }
    } catch (error) {
      console.trace(error);
    }
  };
  const handleBuyComputation = async (e) => {
    reset();
    try {
      e.preventDefault();
      console.log("i am here");
      const iexec = await loadIExec();
      setIsLoading(true);
      const dealId = await buyComputation(iexec, inputContractId, toast);
      const deal = await showDeal(iexec, dealId);
      const taskid = deal.tasks[0];
      await receiveResultFromOffChainOracle(taskid);
      setIsLoading(false);
      toast.success("A deal has successfully been created!", {
        position: "top-left",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      await utils.sleep(9000);
      toast.success(
        "Wait for the result of the computation to be emitted by the iExec Doracle Contract!",
        {
          position: "top-left",
          autoClose: 50000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        }
      );
    } catch (error) {
      console.trace(error);
      setIsLoading(false);
      toast.error(`${error.message}`, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      throw Error(error);
    }
  };

  const CardKeyValue = (props) => (
    <>
      <div className="card-key-value">
        <div>{props.label}</div>
        <div>{props.value}</div>
      </div>
      <hr />
    </>
  );
  const renderTooltip = (props) => (
    <Tooltip id="button-tooltip" {...props}>
      Enter contract Id
    </Tooltip>
  );

  const LoadingView = () => (
    <>
      Loading...
      <Spinner animation="border" variant="light" />
    </>
  );

  function NavBar() {
    return (
      <>
        <div className="minimalistic-nav-bar">
          <div>
            <p className="d-inline-block align-top">MFSSIA</p>
          </div>
          <div> Connection Interface</div>
        </div>
      </>
    );
  }

  return (
    <>
      <NavBar />

      <Container className="square inner-container">
        <br />
        {/* {isNonZeroNumber(userDetails["rewardPerDay"])? <RewardsPhaseActive /> : <RewardsPhaseFinished/>} */}
        <CardKeyValue
          label="Business Contract: "
          value={utils.capitalizeFirstLetter(offchainResultInfo.business_contract)}
        />
        <CardKeyValue
          label="Security License: "
          value={utils.capitalizeFirstLetter(offchainResultInfo.security_lincense)}
        />
        <CardKeyValue 
        label="Gateway: " 
        value={utils.capitalizeFirstLetter(offchainResultInfo.gateway)} 
        />
        <CardKeyValue
          label="Connection State: "
          value={displayConnectionState(offchainResultInfo.connection_state)}
        />
        <CardKeyValue
          label="ConnectionID: "
          value={offchainResultInfo.connectionId}
        />

        <br />
        <br />
        <CardKeyValue label="Enter contract ID to initiate a connection: " />
        <div className="label-above-button"></div>
        <div className="input-button-container">
          <div>
            <Form.Control
              placeholder="contractId"
              value={inputContractId}
              onChange={(event) => setInputContractId(event.target.value)}
            />
            <br />
            <br />
          </div>

          <div>
            {isLoading ? (
              <LoadingView />
            ) : (
              <OverlayTrigger
                placement="right"
                delay={{ show: 250, hide: 400 }}
                overlay={renderTooltip}
              >
                <Button onClick={handleBuyComputation}>submit</Button>
              </OverlayTrigger>
            )}
            <br />
            <br />
            <br />
          </div>
        </div>
        <br />
        <br />
        <CardKeyValue label="Storage Status: " value={ipfsStorage} />
        <Button onClick={handleRemoteStorage}>
          Initialize IPFS Remote Storage
        </Button>
      </Container>
      <ToastContainer
        position="top-right"
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnFocusLoss={false}
        draggable
        pauseOnHover
        transition={Slide}
      />
    </>
  );
}
