/* eslint-disable import/no-anonymous-default-export */
export default function(ms)
{
	return new Promise(resolve => setTimeout(resolve, ms));
}

export const capitalizeFirstLetter = (word) => {
	return word.charAt(0).toUpperCase() + word.slice(1)
   }
  