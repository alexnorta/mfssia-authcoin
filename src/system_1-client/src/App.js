import { useState, useEffect } from "react";
import { ToastContainer, toast } from 'react-toastify';

import serviceAbi from "./abis/serviceconsumer.json";
import "react-toastify/dist/ReactToastify.css";
import {getContractInstance} from './lib/network'
import LoadingView from './Spinner'

const contractInstance = getContractInstance(serviceAbi);

export default function App() {
  const [contractInfo, setContractInfo] = useState({
    contractId: "-",
    price: "-",
    delivery_date: "-",
    quantity: "-",
    product_name: "-",
  });
  const [isGlobalLoading, setIsGlobalLoading] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    (async () => {
      setIsGlobalLoading(false);
      setIsLoading(false)
    })();

  },[]);

  const handleFetchContractInfo = async (e) => {
    e.preventDefault();
    try {
      const data = new FormData(e.target);
      const readContractId = data.get("contractId")
      setIsGlobalLoading(true);
     const results = await contractInstance.getBusinessContractInfo(readContractId)
     const prepareResult = {
       contractId: results[0].toString(),
       price: results[1].toString(),
       quantity: results[2].toString(),
       delivery_date: results[3].toString(),
       product_name: results[4].toString(),
     };
     const contractId = prepareResult.contractId
    const price = prepareResult.price;
    const delivery_date = prepareResult.delivery_date;
    const quantity = prepareResult.quantity;
    const product_name = prepareResult.product_name;
    setIsGlobalLoading(false);
    setContractInfo({
      contractId,
      price,
      quantity,
      delivery_date,
      product_name,
    });
    toast.success("Fetch latest Business Contract info successfully from blockchain!", {
      position: "top-left",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
    } catch (error) {
      toast.error(`${error.message}`, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      setIsGlobalLoading(false);
    }
  
  };

  const handleRegister = async (e) => {
    e.preventDefault();
    const data = new FormData(e.target);
    try {
    console.log('data form', data.get("contractId"))
    const productName = data.get("product_name").toLowerCase()
    setIsLoading(true);
    const result = await contractInstance.addBusinessContractInfo(data.get("contractId"), data.get("price"),data.get("delivery_date"),data.get("quantity"),productName);
    console.log("Debug.result: " + JSON.stringify(result));
    setIsLoading(false);
    toast.success("Business Contract details updated on-chain", {
      position: "top-left",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
    
    } catch (error) {
      console.log('error', error)
      toast.error(`${error.message}`, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      setIsLoading(false);
    }
   
  };

  function Nav() {
    return (
      <div>
        <nav className="bg-gray-800">
          <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <div className="flex items-center justify-between h-16">
              <div className="flex items-center">
                <div className="flex-shrink-0">
                  <img
                    className="h-8 w-8"
                    src="https://tailwindui.com/img/logos/workflow-mark-indigo-500.svg"
                    alt="Workflow"
                  />
                </div>
                <div className="hidden md:block">
                  <div className="ml-10 flex items-baseline space-x-4">
                    <a
                      href="#"
                      className=" hover:bg-gray-700 text-white px-3 py-2 rounded-md text-sm font-medium"
                    >
                      MFSSIA AUTHCOIN BUSINESS CONTRACT REGISTRY
                    </a>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
        </nav>
      </div>
    );
  }
  

  return (
    <div className="grid grid-cols-1 gap-2 md:grid-cols-1">
      <Nav />
      <div>
        <form className="m-4" onSubmit={handleFetchContractInfo}>
          <div className="credit-card w-full lg:w-3/5 sm:w-auto shadow-lg mx-auto rounded-xl bg-white">
            <main className="mt-4 p-4">
              <h1 className="text-xl font-semibold text-gray-700 text-center">
                System_1 Business Contract Service
              </h1>
              <div className="">
                <div className="my-3">
                  <input
                    type="text"
                    name="contractId"
                    className="input input-bordered block w-full focus:ring focus:outline-none"
                    placeholder="Contract Id"
                  />
                </div>
              </div>
            </main>
            <footer className="p-4">
              <button
                disabled={isGlobalLoading}
                type="submit"
                className="btn btn-primary submit-button focus:ring focus:outline-none w-full"
              >
                {isGlobalLoading ? <LoadingView /> : 'Get last business contract Challenge set response'}
              </button>

            </footer>
            <div className="px-4">
              <div className="overflow-x-auto">
                <table className="table w-full">
                  <thead>
                    <tr>
                      <th>ContractId</th>
                      <th>Price</th>
                      <th>Quantity</th>
                      <th>Delivery Interval</th>
                      <th>Product Name</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th>{contractInfo.contractId}</th>
                      <td>{contractInfo.price}</td>
                      <td>{contractInfo.quantity}</td>
                      <td>{contractInfo.delivery_date}</td>
                      <td>{contractInfo.product_name}</td>
                      <td>{contractInfo.state}</td>
                    </tr>
          
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </form>
        <div className="m-4 credit-card w-full lg:w-3/5 sm:w-auto shadow-lg mx-auto rounded-xl bg-white">
          <div className="mt-4 p-4">
            <h1 className="text-xl font-semibold text-gray-700 text-center">
              Business Contract Challenge
            </h1>

            <form onSubmit={handleRegister}>
              <div className="my-3">
                <input
                  type="text"
                  name="contractId"
                  className="input input-bordered block w-full focus:ring focus:outline-none"
                  placeholder="Contract Id"
                />
              </div>
              <div className="my-3">
                <input
                  type="text"
                  name="price"
                  className="input input-bordered block w-full focus:ring focus:outline-none"
                  placeholder="Price"
                />
              </div>
              <div className="my-3">
                <input
                  type="text"
                  name="quantity"
                  className="input input-bordered block w-full focus:ring focus:outline-none"
                  placeholder="Quantity"
                />
              </div>
              <div className="my-3">
                <input
                  type="text"
                  name="delivery_date"
                  className="input input-bordered block w-full focus:ring focus:outline-none"
                  placeholder="Delivery Interval"
                />
              </div>
              <div className="my-3">
                <input
                  type="text"
                  name="product_name"
                  className="input input-bordered block w-full focus:ring focus:outline-none"
                  placeholder="Product Name"
                />
              </div>
              <footer className="p-4">
                <button
                  disabled={isLoading}
                  type="submit"
                  className="btn btn-primary submit-button focus:ring focus:outline-none w-full"
                >
                {isLoading ? <LoadingView /> : 'Update response'}
                </button>
              </footer>
            </form>
          </div>
        </div>
      </div>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  );
}
