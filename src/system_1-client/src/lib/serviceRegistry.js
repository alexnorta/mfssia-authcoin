import {loadWeb3} from './web3'
import ServiceProvider from '../abis/serviceconsumer.json'

const getServiceProviderContractInfoInstance = async () => {
   const abi = ServiceProvider.abi;
   const web3 = await loadWeb3()
   const networkId = await web3.eth.net.getId()
   const deployedNetwork = ServiceProvider.networks[networkId]
   const serviceProviderInstance = new web3.eth.Contract(abi,deployedNetwork.address)

   return serviceProviderInstance
}

export default getServiceProviderContractInfoInstance

