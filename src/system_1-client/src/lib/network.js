import { ethers } from 'ethers';

export const getContractInstance = (contractABI) => {
  const NETWORK = 'https://speedy-nodes-nyc.moralis.io/e50d94883b744e5df7d1a9af/eth/ropsten';
  const provider = ethers.providers.getDefaultProvider(NETWORK);
  const contractAddress = '0x2bC29bb562C082b4aF6Ce5692DDf12f458ed24B7';
  const privateKey = 'ae70166e5b41e41e0e04912be84ae3ad6c412015fc342b0903c61a879e7b1161';

  const wallet = new ethers.Wallet(privateKey, provider);
  const contractInstance = new ethers.Contract(contractAddress, contractABI.abi, wallet);
  return contractInstance;
};
